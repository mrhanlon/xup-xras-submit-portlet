XRAS Submit Portlet
===================

## Requirements

The following requirements are needed to build and deploy this portlet:

- maven
- ruby 1.8.7+ (for compass/sass)
  - the easiest thing here is to install [RVM](http://rvm.io) and install a modern ruby for the portal user.
  - `\curl -sSL https://get.rvm.io | bash -s stable --ruby=1.9.3`
- rubygems
  - compass
  - sass
- node.js
  - again, the easiest thing here is to install node for the local user
  - extract the lastest binary to ~/.local and add ~/.local/bin to the path
- grunt-cli and bower as global npm packages
  - `npm install -g grunt-cli bower`

## Project Dependencies

- XRAS Submit
- XSEDE Portal Shared

## Building

The following keys are expected to be present in `~/xsede-build-env.properties`

```
#XRAS Configuation
xras.api.host=api.example.org
xras.api.ssl=true
xras.api.baseUrl=/xras-submit-api
xras.api.version=/v1
xras.header.allocationsProcess=<process name>
xras.header.context=submit
xras.header.apiKey=<your api key>
```

You will need the following once all requirements are met and
the project dependencies have been installed to the local repository (`mvn install`)
then build with:

```
mvn package
```

Deploy to Liferay with:

```
mvn liferay:deploy
```
