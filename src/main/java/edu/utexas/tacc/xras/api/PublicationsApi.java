/**
 * 
 */
package edu.utexas.tacc.xras.api;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.utexas.tacc.exceptions.QueryException;
import edu.utexas.tacc.hibernate.bean.Publication;
import edu.utexas.tacc.hibernate.dao.PublicationDao;
import edu.utexas.tacc.xras.ServiceException;

/**
 * @author mrhanlon
 *
 */
@Controller
@RequestMapping(value = "/services/publications")
public class PublicationsApi extends BaseApiController {
  
  private static final Logger logger = Logger.getLogger(PublicationsApi.class);

  @Autowired
  private SessionFactory sessionFactory;
  
  /**
   * Look up publications for all the people on a request.
   * @param request
   * @return
   * @throws ServiceException 
   */
  @Transactional
  @RequestMapping(value="/{usernames}", method = RequestMethod.GET)
  public @ResponseBody List<Publication> getXcdbPublications(HttpServletRequest request, @PathVariable String usernames) throws ServiceException {
    logger.debug("calling getXcdbPublications(" + usernames + ")");
    List<Publication> pubs;
    try {
      Session session = sessionFactory.getCurrentSession();
      PublicationDao dao = new PublicationDao(session);
      pubs = dao.getPublicationsByUsernameList(usernames.split(","));
    } catch (QueryException e) {
      logger.error(e);
      throw new ServiceException(e);
    }
    
    // load lazy object before serialization
    for (Publication p : pubs) {
      Hibernate.initialize(p.getAuthors());
      Hibernate.initialize(p.getData());
      Hibernate.initialize(p.getProjects());
    }
    
    return pubs;
  }
}
