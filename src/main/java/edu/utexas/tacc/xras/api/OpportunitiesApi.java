/**
 * 
 */
package edu.utexas.tacc.xras.api;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.utexas.tacc.xras.ServiceException;
import edu.utexas.tacc.xras.api.exception.AuthException;
import edu.utexas.tacc.xras.http.session.XrasSession;
import edu.utexas.tacc.xras.model.Opportunity;
import edu.utexas.tacc.xras.service.OpportunityService;

/**
 * @author mrhanlon
 *
 */
@Controller
@RequestMapping(value="/services/opportunities", method=RequestMethod.GET)
public class OpportunitiesApi extends BaseApiController {

  @RequestMapping
  public @ResponseBody List<Opportunity> opportunities(HttpServletRequest request) throws AuthException, ServiceException {
    XrasSession session = getXrasSession(request);
    return opportunityService.list(session);
  }

  @RequestMapping(value="/{id}")
  public @ResponseBody Opportunity opportunity(HttpServletRequest request, @PathVariable int id) throws AuthException, ServiceException {
    XrasSession session = getXrasSession(request);
    return opportunityService.read(session, new Opportunity(id));
  }
  
  @Autowired
  OpportunityService opportunityService;

}
