/**
 * 
 */
package edu.utexas.tacc.xras.api;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.utexas.tacc.xras.ServiceException;
import edu.utexas.tacc.xras.api.exception.AuthException;
import edu.utexas.tacc.xras.http.session.XrasSession;
import edu.utexas.tacc.xras.model.Resource;
import edu.utexas.tacc.xras.service.ResourceService;

/**
 * @author mrhanlon
 *
 */
@Controller
@RequestMapping(value="/services/resources", method=RequestMethod.GET)
public class ResourcesApi extends BaseApiController {

  @Autowired private ResourceService resourceService;
  
  @RequestMapping
  public @ResponseBody List<Resource> list(HttpServletRequest request) throws ServiceException, AuthException {
    XrasSession session = getXrasSession(request);
    return resourceService.list(session);
  }

  @RequestMapping("/{id}")
  public @ResponseBody Resource get(HttpServletRequest request, @PathVariable int id) throws ServiceException, AuthException {
    XrasSession session = getXrasSession(request);
    return resourceService.read(session, new Resource(id));
  }
  
}
