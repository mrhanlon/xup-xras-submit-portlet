/**
 * 
 */
package edu.utexas.tacc.xras.api;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;

import edu.utexas.tacc.xras.ServiceException;
import edu.utexas.tacc.xras.api.exception.AuthException;
import edu.utexas.tacc.xras.http.BinaryResponse;
import edu.utexas.tacc.xras.http.session.XrasSession;
import edu.utexas.tacc.xras.model.Opportunity;
import edu.utexas.tacc.xras.model.OpportunityAttributeValue;
import edu.utexas.tacc.xras.model.Person;
import edu.utexas.tacc.xras.model.Request;
import edu.utexas.tacc.xras.model.RequestAction;
import edu.utexas.tacc.xras.model.RequestAllocationDate;
import edu.utexas.tacc.xras.model.RequestDocument;
import edu.utexas.tacc.xras.model.RequestFos;
import edu.utexas.tacc.xras.model.RequestGrant;
import edu.utexas.tacc.xras.model.RequestPersonRole;
import edu.utexas.tacc.xras.model.RequestPublication;
import edu.utexas.tacc.xras.model.RequestResource;
import edu.utexas.tacc.xras.model.RequestRole;
import edu.utexas.tacc.xras.model.RequestValidation;
import edu.utexas.tacc.xras.model.ResourceAttributeValue;
import edu.utexas.tacc.xras.model.type.RequestType;
import edu.utexas.tacc.xras.service.RequestDocumentService;
import edu.utexas.tacc.xras.service.RequestService;

/**
 * @author mrhanlon
 *
 */
@Controller
@RequestMapping("/services/requests")
public class RequestsApi extends BaseApiController {
  
  private static final Logger logger = Logger.getLogger(RequestsApi.class);

  @RequestMapping(method=RequestMethod.GET)
  public @ResponseBody Collection<Request> listRequests(HttpServletRequest httpRequest) throws ServiceException, AuthException {
    XrasSession session = getXrasSession(httpRequest);
    return Collections2.filter(requestService.list(session), new Predicate<Request>() {
      @Override
      public boolean apply(Request input) {
        // remove any deleted
        return ! input.isDeleted();
      }
      
    });
  }
  
  @RequestMapping(value="/requirements", method=RequestMethod.GET, produces="application/json")
  public void modelRequirements(HttpServletRequest httpRequest, HttpServletResponse response) throws ServiceException{
    InputStream in = null;
    try {
      in = getClass().getClassLoader().getResourceAsStream("validation.json");
      IOUtils.copy(in, response.getOutputStream());
    } catch (IOException e) {
      throw new ServiceException(e);
    } finally {
      if (in != null) {
        try {
          in.close();
        } catch (IOException e) {
          logger.warn("Failed to close validation.json InputStream", e);
        }
      }
    }
  }

  @RequestMapping(value="/{id}", method=RequestMethod.GET)
  public @ResponseBody Request request(HttpServletRequest httpRequest, @PathVariable int id) throws ServiceException, AuthException {
    XrasSession session = getXrasSession(httpRequest);
    return requestService.read(session, new Request(id));
  }
  
  @RequestMapping(method=RequestMethod.POST)
  public @ResponseBody Request save(HttpServletRequest httpRequest, @RequestBody Request request) throws ServiceException, AuthException {
    XrasSession session = getXrasSession(httpRequest);
    
    Opportunity opportunity = new Opportunity(request.getOpportunityId());
    RequestType requestType = new RequestType();
    requestType.setRequestType(request.getRequestType());
    Request created = requestService.newRequest(session, request, opportunity, requestType);
    
    request.setId(created.getId());
    
    // add first action, if necessary
    RequestAction action = null;
    if (created.getActions().size() == 0) {
      action = new RequestAction();
      action.setActionType(request.getRequestType());
      created = requestService.addAction(session, request, action);
    } else {
      action = created.getActions().get(0);
    }

    // add roles
    saveRequestRoles(session, request, null);
    
    // add fos
    saveRequestFos(session, request, null);
    
    // grants
    saveRequestGrants(session, request, null);
    
    // publications
    saveRequestPublications(session, request, null);
    
    // add/update resources
    saveActionResources(session, request, action, null);
    
    // resource attributes
    saveActionResourceAttributes(session, request, action);
    
    // opportunity attributes
    saveActionOpportunityAttributes(session, request, action);
    
    // documents
    saveActionDocuments(session, request, action, null);
    
    // attributes
    created = requestService.addUpdateAttributes(session, request);
    
    return created;
  }
  
  @RequestMapping(value="/{id}", method=RequestMethod.PUT)
  public @ResponseBody Request update(HttpServletRequest httpRequest, @RequestBody final Request request) throws ServiceException, AuthException {
    XrasSession session = getXrasSession(httpRequest);
    
    final Request prev = requestService.read(session, new Request(request.getId()));
    
    // add/update roles
    saveRequestRoles(session, request, prev);
    
    // add/update fos
    saveRequestFos(session, request, prev);
    
    // grants
    saveRequestGrants(session, request, prev);
    
    // publications
    saveRequestPublications(session, request, prev);
    
    // active action
    final RequestAction action = Iterables.getLast(request.getActions());
    RequestAction prevAction = null;
    if (action.getId() > 0) {
      prevAction = Iterables.find(prev.getActions(), new Predicate<RequestAction>() {

        @Override
        public boolean apply(RequestAction input) {
          return input.getId() == action.getId();
        }
        
      });
    } else {
      // create action
      Request saved = requestService.addAction(session, request, action);
      action.setId(Iterables.getLast(saved.getActions()).getId());
    }
    
    // update action
    requestService.updateAction(session, request, action);
    
    // add/update resources
    saveActionResources(session, request, action, prevAction);
    
    // resource attributes
    saveActionResourceAttributes(session, request, action);
    
    // opportunity attributes
    saveActionOpportunityAttributes(session, request, action);
    
    // documents
    saveActionDocuments(session, request, action, prevAction);
    
    // attributes
    Request updated = requestService.addUpdateAttributes(session, request);
    
    return updated;
  }
  
  private void saveRequestRoles(final XrasSession session, final Request submitted, final Request previous) throws ServiceException {

    Map<Person, List<RequestRole>> addUpdateRoles = new HashMap<Person, List<RequestRole>>();
    Map<Person, List<RequestRole>> removedRoles = new HashMap<Person, List<RequestRole>>();
    
    // iterate through submitted roles and add to person-role mapping
    for (RequestPersonRole personRole : submitted.getRoles()) {
      List<RequestRole> roles = new ArrayList<RequestRole>();
      for (RequestRole role : personRole.getRoles()) {
        role.setAccountToBeCreated(true); // default
        roles.add(role);
      }
      addUpdateRoles.put(personRole.getPerson(), roles);
    }
    
    if (previous != null) {
      // iterate through previous roles to find any that have been removed.
      for (RequestPersonRole personRole : previous.getRoles()) {
        
        if (addUpdateRoles.containsKey(personRole.getPerson())) {
          List<RequestRole> removed = new ArrayList<RequestRole>();
          // if the person still has roles, check for any removed
          for (RequestRole role : personRole.getRoles()) {
            if (! addUpdateRoles.get(personRole.getPerson()).contains(role)) {
              removed.add(role);
            }
          }
          if (removed.size() > 0) {
            removedRoles.put(personRole.getPerson(), removed);
          }
        } else {
          // person no longer has roles, remove all
          removedRoles.put(personRole.getPerson(), personRole.getRoles());
        }
      }
    }
    
    // do add/updates
    for (Person p : addUpdateRoles.keySet()) {
      for (RequestRole r : addUpdateRoles.get(p)) {
        if (r.isNew()) {
          requestService.addRole(session, submitted, r, p);
        } else {
          requestService.updateRole(session, submitted, r);
        }
      }
    }
    
    // do removes
    for (Person p : removedRoles.keySet()) {
      for (RequestRole r : removedRoles.get(p)) {
        requestService.deleteRole(session, submitted, r);
      }
    }
  }

  private void saveRequestFos(final XrasSession session, final Request submitted, final Request previous) throws ServiceException {

    for (Iterator<RequestFos> it = submitted.getFos().iterator(); it.hasNext(); ) {
      RequestFos fos = it.next();
      logger.debug("Updating fos: " + fos.getId());
      requestService.addFos(session, submitted, fos);
    }
    
    // remove previous fos
    if (previous != null) {
      Collection<RequestFos> remFos = Collections2.filter(previous.getFos(), new Predicate<RequestFos>() {
  
        @Override
        public boolean apply(RequestFos input) {
          return ! submitted.getFos().contains(input);
        }
        
      });
      for (Iterator<RequestFos> it = remFos.iterator(); it.hasNext(); ) {
        RequestFos fos = it.next();
        logger.debug("Removing FoS: " + fos.getId());
        requestService.deleteFos(session, submitted, fos);
      }
    }
  }

  private void saveRequestGrants(final XrasSession session, final Request submitted, final Request previous) throws ServiceException {
    for (Iterator<RequestGrant> it = submitted.getGrants().iterator(); it.hasNext(); ) {
      RequestGrant grant = it.next();
      if (grant.isNew()) {
        logger.debug("New grant: " + grant.getTitle());
        requestService.addGrant(session, submitted, grant);
      } else {
        logger.debug("Updating grant ID: " + grant.getId());
        requestService.updateGrant(session, submitted, grant);
      }
    }
    if (previous != null) {
      Collection<RequestGrant> remFos = Collections2.filter(previous.getGrants(), new Predicate<RequestGrant>() {

        @Override
        public boolean apply(RequestGrant input) {
          return ! submitted.getGrants().contains(input);
        }
        
      });
      for (Iterator<RequestGrant> it = remFos.iterator(); it.hasNext(); ) {
        RequestGrant grant = it.next();
        logger.debug("Removing RequestGrant: " + grant.getId());
        requestService.deleteGrant(session, submitted, grant);
      }

    }
  }

  private void saveRequestPublications(final XrasSession session, final Request submitted, final Request previous) throws ServiceException {
    for (Iterator<RequestPublication> it = submitted.getPublications().iterator(); it.hasNext(); ) {
      RequestPublication pub = it.next();
      if (pub.getId() == 0) {
        logger.debug("Adding RequestPublication: " + pub.getId());
        requestService.addPublication(session, submitted, pub);
      }
    }
    
    if (previous != null) {
      Collection<RequestPublication> rem = Collections2.filter(previous.getPublications(), new Predicate<RequestPublication>() {
  
        @Override
        public boolean apply(RequestPublication input) {
          return ! submitted.getPublications().contains(input);
        }
        
      });
      for (Iterator<RequestPublication> it = rem.iterator(); it.hasNext(); ) {
        RequestPublication pub = it.next();
        logger.debug("Removing RequestPublication: " + pub.getId());
        requestService.deletePublication(session, submitted, pub);
      }
    }
  }

  private void saveActionResources(final XrasSession session, final Request request, final RequestAction submitted, final RequestAction previous) throws ServiceException {

    for (Iterator<RequestResource> it = submitted.getResources().iterator(); it.hasNext(); ) {
      RequestResource resource = it.next();
      logger.debug("Add/Update resource: " + resource.getId() + " for action: " + submitted.getInstanceUrl());
      requestService.addUpdateActionResource(session, request, submitted, resource);
    }
    
    // remove resources
    if (previous != null) {
      Collection<RequestResource> rem = Collections2.filter(previous.getResources(), new Predicate<RequestResource>() {
  
        @Override
        public boolean apply(RequestResource input) {
          return ! submitted.getResources().contains(input);
        }
        
      });
      for (Iterator<RequestResource> it = rem.iterator(); it.hasNext(); ) {
        RequestResource res = it.next();
        logger.debug("Removing resource: " + res.getId());
        requestService.deleteActionResource(session, request, submitted, res);
      }
    }
  }

  private void saveActionDocuments(final XrasSession session, final Request request, final RequestAction submitted, final RequestAction previous) throws ServiceException {

    // adds happen on-demand
    
    // remove documents
    if (previous != null) {
      Collection<RequestDocument> rem = Collections2.filter(previous.getDocuments(), new Predicate<RequestDocument>() {
  
        @Override
        public boolean apply(RequestDocument input) {
          return ! submitted.getDocuments().contains(input);
        }
        
      });
      for (Iterator<RequestDocument> it = rem.iterator(); it.hasNext(); ) {
        RequestDocument doc = it.next();
        logger.debug("Removing document: " + doc.getId());
        requestService.deleteActionDocument(session, request, submitted, doc);
      }
    }
  }
  
  private void saveActionResourceAttributes(final XrasSession session, final Request request, final RequestAction action) throws ServiceException {
    
    ServiceException err = null;
    for (ResourceAttributeValue attrValue : action.getResourceAttributes()) {
      try {
        requestService.setActionResourceAttribute(session, request, action, attrValue);
      } catch (ServiceException e) {
        logger.error(e);
        err = e;
      }
    }
    if (err != null) {
      throw err;
    }
  }
  
  private void saveActionOpportunityAttributes(final XrasSession session, final Request request, final RequestAction action) throws ServiceException {
    
    ServiceException err = null;
    for (OpportunityAttributeValue attrValue : action.getOpportunityAttributes()) {
      try {
        requestService.setActionOpportunityAttribute(session, request, action, attrValue);
      } catch (ServiceException e) {
        logger.error(e);
        err = e;
      }
    }
    if (err != null) {
      throw err;
    }
  }
  
  @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
  public @ResponseBody Request deleteRequest(HttpServletRequest httpRequest, @PathVariable int id) throws ServiceException, AuthException {
    XrasSession session = getXrasSession(httpRequest);
    return requestService.delete(session, new Request(id));
  }
  
//  /**
//   * Delete a FoS
//   * @param httpRequest
//   * @param id
//   * @param fosTypeId
//   * @return
//   * @throws ServiceException
//   * @throws AuthException
//   */
//  @RequestMapping(value="/{id}/fos/{fosTypeId}", method=RequestMethod.DELETE)
//  public @ResponseBody Request removeFos(HttpServletRequest httpRequest, @PathVariable int id, @PathVariable int fosTypeId) throws ServiceException, AuthException {
//    XrasSession session = getXrasSession(httpRequest);
//    
//    Request request = requestService.deleteFos(session, new Request(id), new RequestFos(fosTypeId));
//    
//    return request;
//  }

  @RequestMapping(value="/{requestId}/actions", method=RequestMethod.POST)
  public @ResponseBody Request addAction(HttpServletRequest httpRequest, @PathVariable int requestId, @RequestBody final RequestAction action) throws ServiceException, AuthException {
    XrasSession session = getXrasSession(httpRequest);
    
    return requestService.addAction(session, new Request(requestId), action);
  }

  @RequestMapping(value="/{requestId}/actions/{actionId}", method=RequestMethod.PUT)
  public @ResponseBody Request updateAction(HttpServletRequest httpRequest, @PathVariable int requestId, @RequestBody final RequestAction action) throws ServiceException, AuthException {
    XrasSession session = getXrasSession(httpRequest);
    logger.debug(action.toJSON());
    
    Request request = requestService.read(session, new Request(requestId));
    
    if (action.getActionType().equals("Extension")) {
      for (RequestAllocationDate date : action.getAllocationDates()) {
        if (date.isNew()) {
          requestService.addAllocationDate(session, request, action, date);
        } else {
          requestService.updateAllocationDate(session, request, action, date);
        }
      }
      
    } else {
      // "Transfer" or "Supplement"
      RequestAction previous = Iterables.find(request.getActions(), new Predicate<RequestAction>() {
  
        @Override
        public boolean apply(RequestAction input) {
          return action.getId() == input.getId();
        }
        
      });
      saveActionResources(session, request, action, previous);
    }
    
    return requestService.updateAction(session, request, action);
  }
  

  @RequestMapping(value="/{requestId}/actions/{actionId}", method=RequestMethod.DELETE)
  public @ResponseBody Request deleteAction(HttpServletRequest httpRequest, @PathVariable int requestId, @PathVariable int actionId) throws AuthException, ServiceException {
    XrasSession session = getXrasSession(httpRequest);
    
    Request request = new Request(requestId);
    RequestAction action = new RequestAction();
    action.setId(actionId);
    
    return requestService.deleteAction(session, request, action);
  }
  
  /**
   * This is an XCDB query to find the active dates for the *project* that this request relates to.
   * This is only really relevant for extension actions.
   * @param httpRequest
   * @param requestId
   * @return
   * @throws ServiceException
   * @throws AuthException
   */
  @RequestMapping(value="/{requestId}/dates", method=RequestMethod.GET)
  public @ResponseBody Map<String, String> getRequestActiveDates(HttpServletRequest httpRequest, @PathVariable int requestId) throws ServiceException, AuthException {
    XrasSession session = getXrasSession(httpRequest);
    Request request = requestService.read(session, new Request(requestId));
    Map<String, String> requestDates = new HashMap<String, String>();
    
    Connection con = null;
    try {
      ResourceBundle props = ResourceBundle.getBundle("datasource");
      String dsn = props.getString("jndi.name");
      Context context = new InitialContext();
      DataSource ds = (DataSource) context.lookup(dsn);
      con = ds.getConnection();
      PreparedStatement ps = con.prepareStatement("SELECT max(a.end_date) as end_date FROM acct.allocations a, acct.requests rq "
        + "WHERE rq.proposal_number = ? "
        + "AND rq.account_id = a.account_id "
        + "AND rq.start_date = a.initial_start_date "
        + "AND NOT EXISTS (SELECT * FROM acct.requests "
        + "WHERE account_id = rq.account_id "
        + "AND start_date > rq.start_date)");
      ps.setString(1, request.getRequestNumber());
      ResultSet rs = ps.executeQuery();
      if (rs.next()) {
        requestDates.put("endDate", rs.getString("end_date"));
      }
      rs.close();
      ps.close();
    } catch (Exception e) {
      throw new ServiceException(e);
    } finally {
      if (con != null) {
        try {
          con.close();
        } catch (SQLException e) {
          throw new ServiceException(e);
        }
      }
    }
    return requestDates;
  }
  
  /**
   * Delete a role
   * @param httpRequest
   * @param id
   * @param roleId
   * @return
   * @throws ServiceException
   * @throws AuthException
   */
  @RequestMapping(value="/{id}/roles/{roleId}", method=RequestMethod.DELETE)
  public @ResponseBody Request removeRole(HttpServletRequest httpRequest, @PathVariable int id, @PathVariable int roleId) throws ServiceException, AuthException {
    XrasSession session = getXrasSession(httpRequest);
    
    RequestRole role = new RequestRole();
    role.setId(roleId);
    Request request = requestService.deleteRole(session, new Request(id), role);
    
    return request;
  }
  
  /**
   * Delete a grant
   * @param httpRequest
   * @param id
   * @param grantId
   * @return
   * @throws ServiceException
   * @throws AuthException
   */
  @RequestMapping(value="/{id}/grants/{grantId}", method=RequestMethod.DELETE)
  public @ResponseBody Request removeGrant(HttpServletRequest httpRequest, @PathVariable int id, @PathVariable int grantId) throws ServiceException, AuthException {
    XrasSession session = getXrasSession(httpRequest);
    
    RequestGrant grant = new RequestGrant();
    grant.setId(grantId);
    Request request = requestService.deleteGrant(session, new Request(id), grant);
    
    return request;
  }
  
  @RequestMapping(value="/{id}/actions/{actionId}/documents/{documentId}", method=RequestMethod.GET, produces=MediaType.APPLICATION_OCTET_STREAM_VALUE)
  public void getDocumentDownload(HttpServletRequest httpRequest, HttpServletResponse httpResponse, @PathVariable int id, @PathVariable int actionId, @PathVariable int documentId) throws AuthException, ServiceException, IOException {
    XrasSession session = getXrasSession(httpRequest);
    
    Request r = new Request(id);
    RequestAction a = new RequestAction();
    a.setId(actionId);
    RequestDocument d = new RequestDocument();
    d.setId(documentId);
    BinaryResponse resp = documentService.getDocument(session, r, a, d);
    try {
      IOUtils.copy(new ByteArrayInputStream(resp.getResult()), httpResponse.getOutputStream());
      httpResponse.flushBuffer();
    } catch (IOException e) {
      logger.error(e);
      throw e;
    }
  }
  
  @RequestMapping(value = "/{id}/actions/{actionId}/documents", method = RequestMethod.POST)
  public @ResponseBody Request uploadDocument(
    HttpServletRequest httpRequest, @PathVariable int id, @PathVariable int actionId, 
    @RequestParam("title") String title, @RequestParam("documentType") String documentType, @RequestParam("upload") MultipartFile upload
   ) throws AuthException, ServiceException, IOException {
    XrasSession session = getXrasSession(httpRequest);

    File file = null;
    if (!upload.isEmpty()) {
      file = File.createTempFile("xras", ".tmp");
      byte[] bytes = upload.getBytes();
      BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(file));
      stream.write(bytes);
      stream.close();
    }

    Request r = new Request(id);
    RequestAction a = new RequestAction();
    a.setId(actionId);
    RequestDocument d = new RequestDocument();
    d.setTitle(title);
    d.setDocumentType(documentType);
    d.setFilename(upload.getOriginalFilename());

    return requestService.addActionDocument(session, r, a, d, file);
  }
  
  @RequestMapping(value = "/{id}/actions/{actionId}/submit", method = RequestMethod.POST)
  public @ResponseBody Request finalizeRequestAction(HttpServletRequest httpRequest, @PathVariable int id, @PathVariable int actionId) throws AuthException, ServiceException {
    XrasSession session = getXrasSession(httpRequest);
    
    Request r = new Request(id);
    RequestAction a = new RequestAction();
    a.setId(actionId);

    return requestService.submitAction(session, r, a);
  }
  
  @RequestMapping(value = "/{id}/actions/{actionId}/validate", method = RequestMethod.GET)
  public @ResponseBody RequestValidation validateRequestAction(HttpServletRequest httpRequest, @PathVariable int id, @PathVariable int actionId) throws AuthException, ServiceException {
    XrasSession session = getXrasSession(httpRequest);
    
    Request r = new Request(id);
    RequestAction a = new RequestAction();
    a.setId(actionId);

    return requestService.validateAction(session, r, a);
  }

  @Autowired
  private RequestService requestService;

  @Autowired
  private RequestDocumentService documentService;
}
