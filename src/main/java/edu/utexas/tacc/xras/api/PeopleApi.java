/**
 * 
 */
package edu.utexas.tacc.xras.api;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.utexas.tacc.xras.ServiceException;
import edu.utexas.tacc.xras.api.exception.AuthException;
import edu.utexas.tacc.xras.http.session.XrasSession;
import edu.utexas.tacc.xras.model.Person;
import edu.utexas.tacc.xras.service.PersonService;

/**
 * @author mrhanlon
 * 
 */
@Controller
@RequestMapping(value = "/services/people")
public class PeopleApi extends BaseApiController {

  @Autowired
  private PersonService personService;
  
  @RequestMapping(value = "/{username}", method = RequestMethod.GET)
  public @ResponseBody Person get(HttpServletRequest request, @PathVariable String username) throws AuthException, ServiceException {
    XrasSession session = getXrasSession(request);
    return personService.read(session, new Person(username));
  }
  
  @RequestMapping(value = "/{username}", method = RequestMethod.POST)
  public @ResponseBody Person post(HttpServletRequest request, @RequestBody Person person) throws AuthException, ServiceException {
    XrasSession session = getXrasSession(request);
    return personService.save(session, person);
  }

}
