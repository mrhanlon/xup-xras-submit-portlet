/**
 * 
 */
package edu.utexas.tacc.xras.api;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.liferay.portal.model.User;
import com.liferay.portal.util.PortalUtil;

import edu.utexas.tacc.xras.ServiceException;
import edu.utexas.tacc.xras.api.exception.AuthException;
import edu.utexas.tacc.xras.http.session.XrasSession;

/**
 * @author mrhanlon
 *
 */
public abstract class BaseApiController {

  protected XrasSession getXrasSession(HttpServletRequest request) throws AuthException {
    User user = null;
    try {
      user = PortalUtil.getUser(request);
    } catch (Exception e) {
      Logger.getLogger(getClass()).error(e.getMessage());
    }
    if (user != null) {
      String sessionUsername, impersonate = (String) request.getSession().getAttribute("impersonateUser");
      if (impersonate != null) {
        sessionUsername = impersonate;
      } else {
        sessionUsername = user.getScreenName();
      }
      
      return new XrasSession(sessionUsername);
    } else {
      throw new AuthException();
    }
  }
  
  @ExceptionHandler(AuthException.class)
  @ResponseBody
  @ResponseStatus(value= HttpStatus.UNAUTHORIZED)
  public String handleAuthException(AuthException e) {
    Logger.getLogger(getClass()).error(e.getMessage(), e);
    return "Authentication required";
  }

  @ExceptionHandler(ServiceException.class)
  public ResponseEntity<String> handleApiException(HttpServletResponse response, ServiceException e) {
    int statusCode = e.getStatusCode();
    if (statusCode == 0) {
      Throwable t = e;
      while (t.getCause() instanceof ServiceException) {
        statusCode = ((ServiceException) t.getCause()).getStatusCode();
        t = t.getCause();
      }
      if (statusCode == 0) {
        statusCode = 500;
      }
    }
    Logger.getLogger(getClass()).error(e.getMessage(), e);
    return new ResponseEntity<String>(e.getMessage(), HttpStatus.valueOf(statusCode));
  }
  
  @ExceptionHandler(Exception.class)
  @ResponseBody
  @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
  public String handleException(Exception e) {
    Logger.getLogger(getClass()).error(e.getMessage(), e);
    return e.getMessage();
  }
}
