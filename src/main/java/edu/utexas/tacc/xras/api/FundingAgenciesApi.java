/**
 * 
 */
package edu.utexas.tacc.xras.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.utexas.tacc.xras.ServiceException;
import edu.utexas.tacc.xras.api.exception.AuthException;
import edu.utexas.tacc.xras.http.session.XrasSession;
import edu.utexas.tacc.xras.model.FundingAgency;
import edu.utexas.tacc.xras.service.FundingAgencyService;

/**
 * @author mrhanlon
 *
 */
@Controller
@RequestMapping(value="/services/funding_agencies", method=RequestMethod.GET)
public class FundingAgenciesApi extends BaseApiController {

  @Autowired
  private FundingAgencyService faService;
  
  @RequestMapping
  public @ResponseBody List<FundingAgency> list(HttpServletRequest request) throws AuthException, ServiceException {
    XrasSession session = getXrasSession(request);
    return faService.list(session);
  }
  
  @RequestMapping(params="map=true")
  public @ResponseBody Map<Integer, FundingAgency> map(HttpServletRequest request) throws AuthException, ServiceException {
    XrasSession session = getXrasSession(request);
    Map<Integer,FundingAgency> map = new HashMap<Integer, FundingAgency>();
    for (FundingAgency fa : faService.list(session)) {
      map.put(fa.getId(), fa);
    }
    return map;
  }

}
