<h3>XSEDE Resource Allocation System <small>XRAS</small></h3>
<div id="ng-app" ng-app="xrasApp">
  <div class="impersonation" ng-controller="ImpersonationCtrl" ng-include="'views/impersonation.html'"></div>
  <div ng-controller="BreadcrumbsCtrl" ng-include="'views/breadcrumbs.html'"></div>
  <div ng-controller="MessagesCtrl" ng-include="'views/messages.html'"></div>
  <div ng-view></div>
</div>

<!--[if lte IE 7]>
  <script type="text/javascript" src="/xras-submit-portlet/js/json2.js"></script>
<![endif]-->

<!--[if lte IE 9]>
<script type="text/javascript">
  //optional need to be loaded before angular-file-upload-shim(.min).js
  FileAPI = {
    jsPath: '/xras-submit-portlet/js/',
    staticPath: '/xras-submit-portlet/js/'
  }
</script>
<![endif]-->
