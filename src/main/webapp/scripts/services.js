'use strict';

angular.module('xrasApp.service')
  .factory('Notify', function($rootScope) {
    var messages = [];
    return {
      messages: function() {
        return messages;
      },
      clear: function() {
        messages = [];
        $rootScope.$broadcast('xras:notify');
      },
      remove: function(index) {
        messages.splice(index, 1);
      },
      message: function(text, type, icon) {
        messages.push({
          text: text,
          type: type || 'info',
          icon: icon
        });
        $rootScope.$broadcast('xras:notify');
      }
    };
  })
  .factory('Api', function($cacheFactory, Restangular, Liferay) {

    var cache = $cacheFactory('xrasApi');
    Restangular.setDefaultHttpFields({cache: cache});
    if (Liferay && Liferay.ThemeDisplay.getDoAsUserIdEncoded()) {
      // handle Liferay impersonation
      Restangular.setDefaultRequestParams({doAsUserId: Liferay.ThemeDisplay.getDoAsUserIdEncoded()});
    }
    Restangular.addResponseInterceptor(function(data, operation) {
      if (operation === 'put' || operation === 'post' || operation === 'delete' || operation === 'remove') {
        cache.removeAll();
      }
      return data;
    });
    return {
      Opportunities: Restangular.withConfig(function(config) {
        config.setRestangularFields({'id': 'opportunityId'});
      }).service('opportunities'),

      Requests: Restangular.withConfig(function(config) {
        config.setRestangularFields({'id': 'requestId'});
      }).service('requests'),

      Actions: {
        save: function(request, action) {
          if (action.actionId) {
            return request.customPUT(action, 'actions/' + action.actionId);
          } else {
            return request.customPOST(action, 'actions');
          }
        },
        validate: function(request, action) {
          return request.customGET('actions/' + action.actionId + '/validate');
        },
        submit: function(request, action) {
          return request.customPOST({}, 'actions/' + action.actionId + '/submit');
        },
        remove: function(request, action) {
          return request.customDELETE('actions/' + action.actionId);
        }
      },

      Resources: Restangular.withConfig(function(config) {
        config.setRestangularFields({'id': 'resourceId'});
      }).service('resources'),

      People: Restangular.withConfig(function(config) {
        config.setRestangularFields({'id': 'username'});
      }).service('people'),

      FundingAgencies: Restangular.service('funding_agencies'),

      Fos: Restangular.service('types/fos'),

      Roles: Restangular.service('types/roles'),

      Units: Restangular.service('types/units'),

      AttributeTypes: Restangular.service('types/attribute_set_relations'),

      DocTypes: Restangular.service('types/documents'),

      Users: Restangular.service('users'),

      Publications: Restangular.service('publications')
    };
  })
  .factory('RequestUtil', function(_) {
    return {
      /*
       * Returns a lexicographically sortable value for the role. Role names are
       * mapped to numeric ranks, then last name, then first name. For example,
       * the value for 'John Smith, PI' would be '0SmithJohn'.
       */
      requestRoleSort: function(role) {
        var rank;
        switch (role.roles[0].role) {
        case 'PI':
          rank = '0';
          break;
        case 'CoPI':
          rank = '1';
          break;
        case 'Allocation Manager':
          rank = '2';
          break;
        case 'NSF Fellow':
          rank = '3';
          break;
        case 'User':
          rank = '4';
          break;
        default:
          rank = '9';
        }
        return rank + role.person.lastName + role.person.firstName;
      },

      getOriginalAction: function(request) {
        return _.find(request.actions, function(action) {
          return action.actionType === 'New' || action.actionType === 'Renewal';
        });
      },

      requestStatusSort: function(status) {
        return _.indexOf(['Incomplete','Submitted','Under Review','Approved','Rejected'], status);
      },

      isAllowedOperation: function(request, op) {
        return _.contains(request.rules.allowedOperations, op);
      }
    };
  })
  .factory('OpportunityUtil', function(moment) {
    return {
      defaultSort: function(opportunity) {
        if (opportunity.opportunityType === 'Terminating') {
          return opportunity.submissionEndDate;
        } else {
          return opportunity.displayOpportunityName;
        }
      },
      canSubmitNew: function(opportunity) {
        if (opportunity.rules.canSubmitNewRequest && moment().isAfter(opportunity.submissionBeginDate)) {
          var allowed = true;
          if (opportunity.opportunityType === 'Terminating') {
            allowed = moment(opportunity.submissionEndDate).isAfter();
          }
          return allowed;
        }
      },
      allocationTypeSummary: function(opportunity) {
        var summaries = {
          'Research': 'Research allocations may be requested for any compute, visualization, or storage resource and require a formal request document and CVs (for PI/Co-PIs). Research allocations are typically appropriate as follow-ons to Startups; but a PI need not request a Startup prior to submitting a Research request. A PI may submit a Research allocation request at any time during an active Startup allocation, if the PI has fulfilled his or her startup needs. A successful Research allocation will supersede any Startup allocation and start a new 12-month allocation period.',
          'Startup': 'The fastest way to get started on XSEDE, Startup allocations require minimum documentation, are reviewed all year long, and are valid for one year. Startup allocations vary in allotment size, depending on the resources requested. The total Startup allocation across all resources cannot exceed 200,000 SUs. For resource specific Startup limits, please see: <a href="https://www.xsede.org/allocations#sulimits" target="blank">XSEDE Startup limits table</a>.',
          'Campus Champions': 'The Campus Champions program supports campus representatives as a local source of knowledge about high-performance and high-throughput computing and other digital services, opportunities and resources.',
          'Educational': 'Education allocations are for academic or training classes such as those provided at a university that have specific begin and end dates. In addition to training classes, education projects support classroom instruction. Education allocations are appropriate for academic courses or training classes having a registration number, class description, and known timeframe. Education requests have the same allocation size limits as Startup requests. PIs on education allocations must be faculty.',
          'Software Testbeds': 'Testbeds are environments that allow users to conduct scientific experiments and typically consist of a variety of hardware and software components.'
        };
        return summaries[opportunity.allocationType];
      }
    };
  })
  .factory('ResourceAttributeUtil', function(_) {
    return {
      map: function(action, resources) {
        var sorted = [];
        var map = {};
        var resourceArray = _.values(resources);
        var i, j, k;
        for (i = 0; i < resourceArray.length; i++) {
          var res = resourceArray[i];
          for (j = 0; j < res.attributeSets.length; j++) {
            var set = res.attributeSets[j];
            for (k = 0; k < set.attributes.length; k++) {
              var attr = set.attributes[k];
              var val = _.findWhere(action.resourceAttributes, {'resourceId': res.resourceId, 'attributeId': attr.attributeId});
              val = val || {
                resourceId: res.resourceId,
                attributeId: attr.attributeId,
                resourceAttributeId: attr.attributeId,
                attributeValue: ''
              };
              sorted.push(val);
              map[res.resourceId] = map[res.resourceId] || {};
              map[res.resourceId][attr.attributeId] = val;
            }
          }
        }
        action.resourceAttributes = sorted;
        return map;
      }
    };
  })
  .factory('OpportunityAttributeUtil', function(_) {
    return {
      map: function(action, opportunity) {
        var index = _.indexBy(action.opportunityAttributes, 'attributeId');
        var sorted = [];
        var map = {};
        var i, j;
        for (i = 0; i < opportunity.attributeSets.length; i++) {
          var set = opportunity.attributeSets[i];
          for (j = 0; j < set.attributes.length; j++) {
            var attr = set.attributes[j];
            var val = index[attr.attributeId] || {
              attributeId: attr.attributeId,
              opportunityAttributeId: attr.attributeId,
              attributeValue: ''
            };
            sorted.push(val);
            map[attr.attributeId] = val;
          }
        }
        action.opportunityAttributes = sorted;
        return map;
      }
    };
  });
