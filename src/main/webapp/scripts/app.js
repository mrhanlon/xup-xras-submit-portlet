'use strict';

angular.module('xrasApp.service', ['restangular']);
angular.module('xrasApp', [
  'ngRoute',
  'ngAnimate',
  'ngSanitize',
  'ng-breadcrumbs',
  'underscore',
  'jquery',
  'moment',
  'liferay',
  'angularFileUpload',
  'mgo-angular-wizard',
  'xrasApp.service'
]).config(function(RestangularProvider) {
  RestangularProvider.setBaseUrl('/delegate/services');
}).run(function($rootScope, _, Restangular, Api) {
    Api.Users.one().get().then(function(user) {
      $rootScope.user = user;
    });
    Api.Requests.one().one('requirements').get().then(function(resp) {
      $rootScope.requirements = resp;
    });
    $rootScope.isCurrentUser = function (username) {
      return $rootScope.user.username === username;
    };
});
