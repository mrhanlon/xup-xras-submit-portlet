'use strict';
angular.module('xrasApp').controller('RequestFormAdvancedCtrl', function($scope, $routeParams, $location, $q, _, Api, Notify, RequestUtil) {

  // deferreds for all of our lookups
  $scope.loading = true;
  var deferred = {
    roles: $q.defer(),
    fos: $q.defer(),
    fa: $q.defer(),
    units: $q.defer(),
    request: $q.defer()
  };

  // after everything is loaded, emit ready event
  var promises = _.map(deferred, function(d) { return d.promise; });
  $q.all(promises).then(function() {
    $scope.loading = false;
    $scope.$broadcast('xras:requestReady', $scope.request);
  });

  // Types
  $scope.types = {};

  // Roles
  Api.Roles.getList().then(function(list) {
    $scope.types.roles = list;
    deferred.roles.resolve(list);
  });

  // Fields of Science
  Api.Fos.getList().then(function(list) {
    $scope.fields = list;
    deferred.fos.resolve(list);
  });

  // Funding Agencies
  Api.FundingAgencies.getList().then(function(list) {
    $scope.fundingAgencyList = list;
    $scope.fundingAgencyMap = _.indexBy(list, 'fundingAgencyId');
    deferred.fa.resolve(list);
  });

  // Type Units
  Api.Units.getList().then(function(list) {
    $scope.types.units = list;
    deferred.units.resolve(list);
  });

  Api.Requests.one($routeParams.requestId).get().then(
    function(req) {
      $scope.request = req;
      $scope.action = RequestUtil.getOriginalAction($scope.request);
      Api.Opportunities.one(req.opportunityId).get().then(function(opp) {
        $scope.opportunity = opp;
        deferred.request.resolve(req);
      });
    },
    function(error) {
      if (error.status === 404) {
        Notify.message('The Request was not found!', 'error');
      } else {
        Notify.message('There was and error loading the Request. Please try again.', 'error');
      }
      $location.path('/');
    }
  );

  // final Save handler
  $scope.saveRequest = function() {
    if ($scope.saving) {
      return;
    }
    $scope.saving = true;
    var promise;
    if ($scope.request.requestId) {
      promise = $scope.request.save();
    } else {
      promise = Api.Requests.post($scope.request);
    }
    promise.then(
      function(resp) {
        $scope.saving = false;
        Notify.message('Request saved!', 'success', 'thumbs-up');
        $location.path('/requests/' + resp.requestId);
      }, function(error) {
        console.log(error);
      }
    );
  };

  $scope.cancel = function() {
    if ($scope.saving) {
      return;
    }
    if ($scope.request.requestId) {
      $location.path('/requests/' + $scope.request.requestId);
    } else {
      $location.path('/');
    }
  };

  $scope.guided = function() {
    $location.path('/requests/' + $scope.request.requestId + '/edit');
  };

});
