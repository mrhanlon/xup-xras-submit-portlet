'use strict';
angular.module('xrasApp')

  // Request Display
  .controller('RequestCtrl', function (_, $scope, $routeParams, $q, $location, Notify, Api, RequestUtil) {

    $scope.loading = true;

    // load all the things!
    var deferred = {
      fos: $q.defer(),
      fa: $q.defer(),
      request: $q.defer(),
      opportunity: $q.defer(),
    };

    Api.Fos.one().get({map:true}).then(function(fos) {
      $scope.fields = fos;
      deferred.fos.resolve(fos);
    });

    Api.FundingAgencies.one().get({map:true}).then(function(map) {
      $scope.fundingAgencies = map;
      deferred.fa.resolve(map);
    });

    Api.Requests.one($routeParams.requestId).get().then(
      function(req) {
        $scope.request = req;
        $scope.allowedOps = {};
        _.each(req.rules.allowedOperations, function(op) {
          $scope.allowedOps['can'+op] = true;
        });

        $scope.existingActions = {};
        _.each(req.rules.existingActions, function(existingAction) {
          var ops = $scope.existingActions[existingAction.actionId] = {};
          if (existingAction.actionType !== 'New' && existingAction.actionType !== 'Renewal') {
            if (_.contains(existingAction.allowedOperations, 'Edit')) {
              ops.canEdit = true;
            }
            if (_.contains(existingAction.allowedOperations, 'Delete')) {
              ops.canDelete = true;
            }
          }
        });

        // get resource info
        var selectedResources = $scope.selectedResources = {};
        _.each(req.actions, function(action) {
          var ram = {};
          _.each(action.resourceAttributes, function(attr) {
            ram[attr.resourceId] = ram[attr.resourceId] || {};
            ram[attr.resourceId][attr.attributeId] = attr;
          });
          action.resourceAttributes = ram;
          selectedResources[action.actionId] = {};
          _.each(action.resources, function(r) {
            var p = Api.Resources.one(r.resourceId).get();
            p.then(function(res) {
              selectedResources[action.actionId][res.resourceId] = res;
            });
          });
        });

        Api.Opportunities.one(req.opportunityId).get().then(function(opp) {
          $scope.opportunity = opp;

          _.each(req.actions, function(action) {
            action.opportunityAttributes = _.indexBy(action.opportunityAttributes, 'attributeId');
          });

          deferred.opportunity.resolve(opp);
        });

        deferred.request.resolve(req);
      },
      function(error) {
        if (error.status === 404) {
          Notify.message('The Request was not found!', 'error');
        } else {
          Notify.message('There was and error loading the Request. Please try again.', 'error');
        }
        $location.path('/');
      }
    );

    $q.all(_.map(deferred, function(d) { return d.promise; })).then(function() {
      $scope.loading = false;
    });

    $scope.validateRequest = function($event, request) {
      if ($event) {
        $event.preventDefault();
      }

      var deferred = $q.defer();
      var action = RequestUtil.getOriginalAction(request);
      Api.Actions.validate(request, action).then(
        function(result) {
          if (result.validation === 'failed') {
            _.each(result.errors, function(error) {
              Notify.message(error, 'error', 'minus-sign');
            });
            deferred.reject(result);
          } else {
            if ($event) {
              Notify.message('Your request validated successfully! You are ready to submit your request for review.', 'success', 'thumbs-up');
            }
            deferred.resolve(result);
          }
        },
        function(error) {
          Notify.message('An unexpected error occurred. We were unable to validate your request. If this problem continues, please contact the Help Desk.', 'error', 'warning-sign');
          deferred.reject(error);
        }
      );

      return deferred.promise;
    };

    $scope.finalizeRequest = function($event, request) {
      $event.preventDefault();
      $scope.validateRequest(null, request).then(function() {
        var action = RequestUtil.getOriginalAction(request);
        Api.Actions.submit(request, action).then(
          function() {
            Notify.message('Your request has been submitted!', 'success', 'thumbs-up');
          },
          function() {
            Notify.message('An unexpected error occurred. We were unable to submit your request. If this problem continues, please contact the Help Desk.', 'error', 'warning-sign');
          }
        );
      });
    };

    $scope.deleteRequest = function(request) {
      if (window.confirm('Are you sure you want to delete this request?')) {
        Api.Requests.one(request.requestId).remove().then(
          function() {
            $location.path('/');
            Notify.message('The request was deleted!', 'success', 'thumbs-up');
          },
          function(error) {
            // TODO
            console.log(error);
            window.alert('There was an error deleting this request. Please try again.');
          }
        );
      }
    };

    $scope.deleteAction = function($event, action) {
      if (window.confirm('Are you sure you want to delete this ' + action.actionType + '?')) {
        Api.Actions.remove($scope.request, action).then(
          function(request) {
            $scope.request = request;
          },
          function(error) {
            // TODO
            console.log(error);
            window.alert('There was an error deleting this action. Please try again.');
          }
        );
      }
    };

    $scope.requestRoleSort = RequestUtil.requestRoleSort;
  });
