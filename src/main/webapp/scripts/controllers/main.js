/**
 * file: controllers/main.js
 * Controller for the default view. Displays currently available Opportunities
 * and Requests.
 */

'use strict';
angular.module('xrasApp').controller('MainCtrl', function ($scope, _, Api, OpportunityUtil, RequestUtil, Notify) {
  $scope.loading = {
    opportunities: true,
    requests: true
  };

  Api.Opportunities.getList().then(function(list) {
    $scope.loading.opportunities = false;
    $scope.opportunities = list;
  }, function(error) {
    console.log(error);
    $scope.loading.opportunities = false;
    $scope.opportunities = [];
    Notify.message('There was an error loading Opportunities!', 'error');
  });

  Api.Requests.getList().then(function(list) {
    $scope.loading.requests = false;
    $scope.requests = list;
    $scope.statuses = _.chain(list).pluck('requestStatus').uniq().sortBy(RequestUtil.requestStatusSort).value();
    $scope.requestFilter = $scope.statuses[0];

    $scope.requestOpportunities = {};
    _.chain(list).pluck('opportunityId').each(function(oppId) {
      Api.Opportunities.one(oppId).get().then(function(opportunity) {
        $scope.requestOpportunities[oppId] = opportunity;
      });
    });

  }, function(error) {
    console.log(error);
    $scope.loading.requests = false;
    $scope.requests = [];
    Notify.message('There was an error loading Requests!', 'error');
  });

  $scope.opportunitySort = OpportunityUtil.defaultSort;

  $scope.canSubmitNew = OpportunityUtil.canSubmitNew;

  $scope.requestRoleSort = RequestUtil.requestRoleSort;

  $scope.allowRequestOp = RequestUtil.isAllowedOperation;

  $scope.filterRequestList = function(status) {
    $scope.requestFilter = status;
  };

});
