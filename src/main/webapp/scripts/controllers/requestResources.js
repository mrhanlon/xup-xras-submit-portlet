'use strict';
angular.module('xrasApp').controller('RequestFormResourcesCtrl',
  function($scope, $q, _, Api, RequestUtil, ResourceAttributeUtil) {

    $scope.$on('xras:requestReady', function() {

      var request,
        action,
        opportunityResourceIndex,
        selectedResources,
        removedResources,
        processSelection,
        resourceAttributesForm;

      request = $scope.request;
      action = $scope.action;
      $scope.opportunityResourceIndex = opportunityResourceIndex = _.indexBy($scope.opportunity.resources, 'resourceId');
      $scope.selectedResources = selectedResources = {};
      removedResources = {};
      $scope.resourceAttributeMap = {};

      resourceAttributesForm = function() {
        $scope.resourceAttributeMap = ResourceAttributeUtil.map(action, selectedResources);
      };

      processSelection = function(resource, select) {
        var deferred = $q.defer(), i;

        // safety first!
        selectedResources[resource.resourceId] = selectedResources[resource.resourceId] || {};
        selectedResources[resource.resourceId].selected = select;
        if (select) {
          // check if already selected
          i = _.map(action.resources, function(r) { return r.resourceId; }).indexOf(resource.resourceId);
          if (i === -1) {
            // add resource
            action.resources.push(removedResources[resource.resourceId] || {
              resourceId: resource.resourceId,
              resourceName: resource.resourceName
            });
          }

          if (selectedResources[resource.resourceId].resourceId) {
            deferred.resolve(selectedResources[resource.resourceId]);
          } else {
            // load rest of resource information
            Api.Resources.one(resource.resourceId).get().then(function(resp) {
              _.extend(selectedResources[resource.resourceId], resp);
              deferred.resolve(selectedResources[resource.resourceId]);
            });
          }
        } else {
          // un-select resource
          i = _.map(action.resources, function(r) { return r.resourceId; }).indexOf(resource.resourceId);
          if (i > -1) {
            // cache entered values
            removedResources[resource.resourceId] = action.resources.splice(i, 1)[0];
          }

          deferred.resolve(selectedResources[resource.resourceId]);
        }

        $scope.$emit('xras:requestChanged', { key: 'resources' });
        return deferred.promise;
      };

      $scope.dependenciesMet = function(resource) {
        var met = opportunityResourceIndex[resource.resourceId].requiredResources.length === 0;
        if (! met) {
          _.each(opportunityResourceIndex[resource.resourceId].requiredResources, function(r) {
            met = met || selectedResources[r.resourceId] && selectedResources[r.resourceId].selected;
          });
        }
        return met;
      };

      $scope.selectResource = function(resource) {

        // safety first!
        selectedResources[resource.resourceId] = selectedResources[resource.resourceId] || {};
        if (selectedResources[resource.resourceId].required) {
          // this resource is required by another
          return;
        } else {
          selectedResources[resource.resourceId].selected = ! selectedResources[resource.resourceId].selected;
          processSelection(resource, selectedResources[resource.resourceId].selected).then(resourceAttributesForm);
        }
      };

      // get action currently selected resources;
      $q.all(_.map(action.resources, function(r) {
        var p = Api.Resources.one(r.resourceId).get();
        p.then(function(res) {
          selectedResources[r.resourceId] = _.extend({selected: true}, res);
        });
        return p;
      })).then(resourceAttributesForm);
    });
  }
);
