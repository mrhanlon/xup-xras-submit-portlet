'use strict';
angular.module('xrasApp')
  .controller('MessagesCtrl', function ($scope, Notify) {
    $scope.$on('xras:notify', function() {
      $scope.messages = Notify.messages();
    });

    $scope.close = function(index) {
      Notify.remove(index);
      $scope.messages = Notify.messages();
    };
  });
