'use strict';
angular.module('xrasApp').controller('StartRequestCtrl', function ($scope, $routeParams, $location, Api, OpportunityUtil) {
  var opportunityId = $routeParams.opportunityId,
    renewalId = $routeParams.renewalId;

  var promise = Api.Opportunities.one(opportunityId).get();
  promise.then(function() {
    $scope.summaryInfo = OpportunityUtil.allocationTypeSummary($scope.opportunity);
  });
  $scope.opportunity = promise.$object;

  $scope.renewalId = renewalId;
  if (renewalId) {
    $scope.prevRequest = Api.Requests.one(renewalId).get().$object;
  }

  $scope.createRequest = function(advanced) {
    var req = {
      opportunityId: opportunityId,
      requestType: 'New'
    };
    if (renewalId) {
      req.requestType = 'Renewal';
      req.requestNumber = $scope.prevRequest.requestNumber;
      // TODO include everything else?
    }
    Api.Requests.post(req).then(function(resp) {
      var formUrl = '/requests/' + resp.requestId + (advanced ? '/edit/advanced' : '/edit');
      $location.path(formUrl);
    }, function() {
      window.alert('There was an error creating your Request! Please try again.');
    });
  };
});
