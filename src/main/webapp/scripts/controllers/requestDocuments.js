'use strict';
angular.module('xrasApp').controller('RequestFormDocumentsCtrl', function($scope, $q, $upload, _, Api) {

  function init() {
    var request = $scope.request,
      action = $scope.action;

    Api.DocTypes.getList().then(function(list) {
      $scope.docTypes = list;
      $scope.docTypesIndex = _.indexBy(list, 'documentType');

      Api.Requests.one().one('requirements').get().then(function(requirements) {
        var allocationType, actionType, docreqs, coPis;

        allocationType = $scope.opportunity.allocationType.toLowerCase().replace(/\s/g, '_');
        actionType = $scope.request.requestType.toLowerCase();
        docreqs = requirements[allocationType][actionType].documents || [];
        coPis = _.filter($scope.request.roles, function(role) {
          return _.where(role.roles, {role: 'CoPI'}).length > 0;
        });

        docreqs.push('CV_PI');
        if (coPis.length > 0) {
          docreqs.push('CV_CoPI');
        }

        $scope.documentRequirements = _.chain($scope.docTypes)
          .filter(function(docType) {
            return _.contains(docreqs, docType.documentType);
          })
          .pluck('displayDocumentType')
          .value();
      });
    });

    $scope.editing = null;

    $scope.downloadDocumentUrl = function(doc) {
      return Api.Requests.one(request.requestId).one('actions', action.actionId).one('documents', doc.documentId).getRestangularUrl();
    };

    $scope.removeDocument = function($index, doc) {
      if (window.confirm('Are you sure you want to the document "' + doc.title +'"?')) {
        action.documents.splice($index,1);
        $scope.$emit('xras:requestChanged', { key: 'documents' });
      }
    };

    $scope.addDocument = function() {
      action.documents = action.documents || [];
      $scope.editing = {
        $index: action.documents.length
      };
    };

    $scope.onFileSelect = function($files) {
      $scope.$files = $files;
    };

    $scope.uploadDocument = function() {
      $scope.upload = $upload.upload({
        url: Api.Requests.one(request.requestId).one('actions', action.actionId).one('documents').getRestangularUrl(),
        method: 'POST',
        data: $scope.editing,
        file: $scope.$files[0],
        fileFormDataName: 'upload'
      }).success(function(data) {
        _.extend($scope.action.documents, _.findWhere(data.actions, { actionId: $scope.action.actionId }).documents);
        $scope.editing = null;
        $scope.$emit('xras:requestChanged', { key: 'documents' });
      });
    };

    $scope.cancelUpload = function() {
      $scope.editing = null;
    };

    $scope.convertBytes = function(bytes) {
      if (bytes < 1000) {
        return bytes + ' bytes';
      }
      var units = ['kB','MB','GB','TB','PB','EB','ZB','YB'];
      var u = -1;
      do {
        bytes = bytes / 1000;
        u = u+1;
      } while (bytes >= 1000);
      return bytes.toFixed(1) + ' ' + units[u];
    };
  }

  $scope.$on('xras:requestReady', function() {
    init();
  });
});
