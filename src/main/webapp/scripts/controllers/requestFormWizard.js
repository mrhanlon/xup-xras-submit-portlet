'use strict';
angular.module('xrasApp').controller('RequestFormWizardCtrl',
  function($scope, $routeParams, $location, $q, $anchorScroll, WizardHandler, _, Api, Notify, RequestUtil) {

    // load all the things!
    $scope.loading = true;
    var deferred = {
      roles: $q.defer(),
      fos: $q.defer(),
      fa: $q.defer(),
      units: $q.defer(),
      request: $q.defer()
    };

    // after everything is loaded, emit ready event
    var promises = _.map(deferred, function(d) { return d.promise; });
    $q.all(promises).then(function() {
      $scope.loading = false;
      setTimeout(function() {
        $scope.$broadcast('xras:requestReady', $scope.request);
      });
    });

    $scope.wizardStep = '';

    // Types
    $scope.types = {};

    // Roles
    Api.Roles.getList().then(function(list) {
      $scope.types.roles = list;
      deferred.roles.resolve(list);
    });

    // Fields of Science
    Api.Fos.getList().then(function(list) {
      $scope.fields = list;
      deferred.fos.resolve(list);
    });

    // Funding Agencies
    Api.FundingAgencies.getList().then(function(list) {
      $scope.fundingAgencyList = list;
      $scope.fundingAgencyMap = _.indexBy(list, 'fundingAgencyId');
      deferred.fa.resolve(list);
    });

    // Type Units
    Api.Units.getList().then(function(list) {
      $scope.types.units = list;
      deferred.units.resolve(list);
    });

    Api.Requests.one($routeParams.requestId).get().then(
      function(req) {
        $scope.request = req;
        $scope.action = RequestUtil.getOriginalAction($scope.request);
        Api.Opportunities.one(req.opportunityId).get().then(function(opp) {
          $scope.opportunity = opp;
          deferred.request.resolve(req);
        });
      },
      function(error) {
        if (error.status === 404) {
          Notify.message('The Request was not found!', 'error');
        } else {
          Notify.message('There was and error loading the Request. Please try again.', 'error');
        }
        $location.path('/');
      }
    );

    // events

    $scope.$on('xras:requestChanged', function(e, data) {
      if (data) {
        $scope.requestForm.$setDirty();
      }
    });

    $scope.validation = {
      status: '',
      errors: []
    };

    $scope.$on('wizard:stepChanged', function(e, data) {
      $anchorScroll();
      if (data.step.wzTitle === 'Submit') {
        $scope.validation.status = 'validating';
        $scope.validation.errors = [];
        Api.Requests.one($scope.request.requestId).one('actions', RequestUtil.getOriginalAction($scope.request).actionId).one('validate').get().then(
          function(result) {
            if (result.validation === 'failed') {
              $scope.validation.status = 'invalid';
              $scope.validation.errors = result.errors;
            } else {
              $scope.validation.status = 'valid';
              $scope.validation.errors = [];
            }
          }
        );
      }
    });

    $scope.saveAndStep = function(forward) {
      if ($scope.requestForm.$dirty) {
        $scope.savingChanges = true;
        var promise;
        if ($scope.request.requestId) {
          promise = $scope.request.save();
        } else {
          promise = Api.Requests.post($scope.request);
        }
        promise.then(
          function(req) {
            $scope.request = req;
            $scope.action = RequestUtil.getOriginalAction($scope.request);
            $scope.$broadcast('xras:requestReady', $scope.request);
            $scope.savingChanges = false;
            $scope.requestForm.$setPristine();
            if (forward) {
              WizardHandler.wizard().next();
            } else {
              WizardHandler.wizard().previous();
            }
          }, function() {
            Notify.message('There was an error saving your changes!', 'error', 'warning-sign');
            $scope.savingChanges = false;
            $anchorScroll();
          }
        );
      } else {
        if (forward) {
          WizardHandler.wizard().next();
        } else {
          WizardHandler.wizard().previous();
        }
      }
    };

    $scope.advanced = function() {
      $location.path($location.path() + '/advanced');
    };

    $scope.submitRequest = function() {

      // validate once more to be sure!
      var action = RequestUtil.getOriginalAction($scope.request);

      Api.Requests.one($scope.request.requestId).one('actions', action.actionId).one('submit').post().then(
        function() {
          Notify.message('Your request has been submitted!', 'success', 'thumbs-up');
          $location.path('/requests/' + $scope.request.requestId);
        },
        function() {
          Notify.message('An unexpected error occurred! If this problem continues please contact the Help Desk.', 'error', 'warning-sign');
        }
      );
    };
  }
);
