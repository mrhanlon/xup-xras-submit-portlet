'use strict';
angular.module('xrasApp').controller('RequestFormGrantsCtrl', function($scope) {

  $scope.editing = null;

  // add grant
  $scope.add = function() {
    $scope.request.grants = $scope.request.grants || [];
    $scope.editing = {
      $index: $scope.request.grants.length
    };
  };

  $scope.edit = function($index, grant) {
    $scope.editing = angular.copy(grant);
    $scope.editing.$index = $index;
  };

  $scope.submit = function() {
    $scope.request.grants.splice($scope.editing.$index, 1, $scope.editing);
    $scope.editing = null;
    $scope.$emit('xras:requestChanged', { key: 'grants' });
  };

  $scope.cancel = function() {
    $scope.editing = null;
  };

  // remove grant
  $scope.remove = function($index, grant) {
    if (window.confirm('Are you sure you want to the grant "' + grant.title +'"?')) {
      $scope.request.grants.splice($index, 1);
      $scope.$emit('xras:requestChanged', { key: 'grants' });
    }
  };
});
