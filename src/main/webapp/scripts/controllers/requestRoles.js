'use strict';
angular.module('xrasApp').controller('RequestFormRoleCtrl', function($scope, _, Api) {

  $scope.$on('xras:requestReady', function(e, data) {
    $scope.request = data;
    var rejectRoles = ['User', 'NSF Fellow'];
    $scope.availableRoles = _.reject($scope.types.roles, function(role) {
      return _.contains(rejectRoles, role.roleType);
    });
  });

  $scope.removeRole = function(role) {
    var index = _.indexOf($scope.request.roles, role);
    if (role.roleId) {
      if (window.confirm('Are you sure you want to remove this Person from this request?')) {
        $scope.request.roles.splice(index, 1);
        $scope.$emit('xras:requestChanged', { key: 'roles' });
      }
    } else {
      $scope.request.roles.splice(index, 1);
      $scope.$emit('xras:requestChanged', { key: 'roles' });
    }
  };

  $scope.isNotUser = function(role) {
    return ! _.chain(role.roles).pluck('role').contains('User').value();
  };

  $scope.search = {};

  $scope.singlePiRole = function(personRole) {
    return function(role) {
      if (! _.findWhere(personRole.roles, {role: 'PI'})) {
        if (role.roleType === 'PI') {
          return ! _.find($scope.request.roles, function(personRole) {
            return _.findWhere(personRole.roles, { role: 'PI' });
          });
        }
        return true;
      }
      return true;
    };
  };

  $scope.addRole = function() {

    if (! $scope.search.username) {
      return;
    }

    $scope.loading = true;
    Api.People.one($scope.search.username).get().then(
      function(person) {
        $scope.loading = false;
        $scope.request.roles = $scope.request.roles || []; // is first role!
        var alreadyAdded = _.find($scope.request.roles, function(role) { return role.person.username === person.username; });
        if (! alreadyAdded) {
          var role = {
            roles: [{ role: '' }],
            person: person
          };
          $scope.request.roles.push(role);
          $scope.$emit('xras:requestChanged', { key: 'roles' });
          $scope.search.username = null;
        } else {
          window.alert('This person has already been added to the request as ' + _.pluck(alreadyAdded.roles, 'role').join(', ') + '!');
        }
      },
      function(error) {
        $scope.loading = false;
        if (error.status === 500) {
          window.alert('There was an error looking up user information! Please try again.');
        } else {
          window.alert('Unable to find a user with this username! Please check the username and try again.');
        }
      }
    );
  };
});
