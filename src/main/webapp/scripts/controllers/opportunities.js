'use strict';
angular.module('xrasApp').controller('OpportunityCtrl', function ($scope, $routeParams, $location, Api, Notify, OpportunityUtil) {
  Api.Opportunities.one($routeParams.opportunityId).get().then(function(opp) {
    $scope.opportunity = opp;
    $scope.canSubmit = OpportunityUtil.canSubmitNew(opp);
  }, function(error) {
    if (error.status === 404) {
      Notify.message('The Opportunity was not found!', 'error');
    } else {
      Notify.message('There was and error loading the Opportunity. Please try again.', 'error');
    }
    $location.path('/');
  });

  $scope.showResourceDetails = {};
  $scope.toggleResourceDetails = function(resourceId) {
    $scope.showResourceDetails[resourceId] = ! $scope.showResourceDetails[resourceId];
  };
});
