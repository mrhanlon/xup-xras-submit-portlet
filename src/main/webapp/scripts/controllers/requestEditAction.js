'use strict';
angular.module('xrasApp').controller('RequestEditActionCtrl', function($scope, $routeParams, $location, $q, moment, _, Api, Notify) {

  /**
   * Loading Tasks
   * 0. request
   * 1. opportunity
   * 2. current_dates (only for extensions, otherwise null task)
   */
  var tasks = [ $q.defer(), $q.defer(), $q.defer() ];

  $scope.loaded = false;

  Api.Requests.one($routeParams.requestId).get().then(function(request) {
    $scope.request = request;
    $scope.action = _.findWhere(request.actions, { actionId: parseInt( $routeParams.actionId, 10 ) });

    Api.Opportunities.one(request.opportunityId).get().then(function(opp) {
      $scope.opportunity = opp;
      tasks[1].resolve(opp); // Task 1.
    }, function(error) {
      tasks[1].reject(error); // Task 1. FAIL
    });

    if ($scope.action.actionType === 'Extension') {
      $scope.action.allocationDates[0] = $scope.action.allocationDates[0] || {
        allocationDateType: 'Requested',
        endDate: ''
      };

      Api.Requests.one(request.requestId).one('dates').get().then(
        function(dates) {
          dates.extensions = [{
            label: '3 Months',
            date: moment(dates.endDate).add('months', 3).format('YYYY-MM-DD')
          }, {
            label: '6 Months',
            date: moment(dates.endDate).add('months', 6).format('YYYY-MM-DD')
          }];
          $scope.dates = dates;
          tasks[2].resolve(dates); // Task 2.
        },
        function(error) {
          // TODO
          console.log(error);
          tasks[2].reject(error); // Task 2. FAIL
        }
      );
    } else {
      tasks[2].resolve(null);
    }

    tasks[0].resolve(request); // Task 0.
  }, function(error) {
    tasks[0].reject(error); // Task 0. FAIL
  });

  $q.all(_.pluck(tasks, 'promise')).then(function() {
    $scope.loaded = true;
    setTimeout(function() {
      $scope.$broadcast('xras:requestReady', $scope.request);
    });
  });

  // events

  $scope.validateAndSubmit = function() {
    Api.Actions.save($scope.request, $scope.action)
      .then(function() {
        return Api.Actions.validate($scope.request, $scope.action);
      })
      .then(function(result) {
        if (result.validation === 'failed') {
          _.each(result.errors, function(error) {
            Notify.message(error, 'error', 'minus-sign');
          });
          return $q.reject(result);
        } else {
          return Api.Actions.submit($scope.request, $scope.action);
        }
      })
      .then(function() {
        Notify.message('Your ' + $scope.action.actionType + ' request has been submitted!', 'success', 'thumbs-up');
        $location.path('/requests/' + $scope.request.requestId);
      })
      .then(null, function(error) {
        console.log(error);
        // TODO
      });
  };

  $scope.saveAction = function() {
    Api.Actions.save($scope.request, $scope.action)
      .then(function() {
        Notify.message('Your ' + $scope.action.actionType + ' request has been saved! You will need to return to submit it before it will be reviewed.', 'success', 'thumbs-up');
        $location.path('/requests/' + $scope.request.requestId);
      }, function(error) {
        console.log(error);
        // TODO
      });
  };

});
