'use strict';
angular.module('xrasApp')
.directive('twbsTooltip', function($) {
  // call the Bootstrap tooltip plugin on the element
  return function(scope, el, attrs) {
    $(el).tooltip({ placement: attrs.placement || 'top'});
  };
})
.directive('ngEnter', function () {
  return function (scope, element, attrs) {
    element.bind('keydown keypress', function (event) {
      if(event.which === 13) {
        scope.$apply(function (){
          scope.$eval(attrs.ngEnter);
        });
        event.preventDefault();
      }
    });
  };
})
.directive('xrasAttrSetForm', function($compile, Api) {
  var handler = function(scope) {
    Api.AttributeTypes.one().get().then(function(map) {
      var template = '',
          type = map[scope.attributeSet.attributeSetRelationTypeId];

      switch (type.attributeSetRelationType) {
      case 'text':
        template = 'views/attributeSets/textFieldForm.html';
        break;
      case 'textarea':
        template = 'views/attributeSets/textAreaForm.html';
        break;
      case 'multi_sel':
        template = 'views/attributeSets/multiSelectForm.html';
        break;
      case 'single_sel':
        template = 'views/attributeSets/selectForm.html';
        break;
      }

      scope.template = template;
    });
  };

  return {
    restrict: 'A',
    replace: true,
    link: handler,
    scope: {
      attributeSet: '=',
      values: '='
    },
    template: '<div ng-include="template"></div>'
  };

})
.directive('xrasAttrSetDisplay', function($compile, Api, _) {

  var handler = function(scope) {

    Api.AttributeTypes.one().get().then(function(map) {
      var template = '',
        type = map[scope.attributeSet.attributeSetRelationTypeId];

      switch (type.attributeSetRelationType) {
      case 'text':
      case 'textarea':
        template = 'views/attributeSets/textDisplay.html';
        break;
      case 'multi_sel':
      case 'single_sel':
        var selectedIds = _.pluck(
          _.filter(scope.values, function(val) {
            return val.attributeValue;
          }),
          'attributeId');
        scope.selectedAttributes = _.pluck(
          _.filter(scope.attributeSet.attributes, function(attr) {
            return _.contains(selectedIds, attr.attributeId);
          }), 'attributeName')
          .join(', ');

        template = 'views/attributeSets/selectDisplay.html';
        break;
      }
      scope.template = template;
    });

  };

  return {
    restrict: 'A',
    replace: true,
    link: handler,
    scope: {
      attributeSet: '=',
      values: '='
    },
    template: '<tr ng-include="template"></tr>'
  };
})
.directive('xrasCitation', function() {
  return {
    restrict: 'A',
    scope: {
      publication: '='
    },
    templateUrl: 'views/publication.html'
  };
})
.directive('xrasRequirements', function(Api, _) {
  var handler = function(scope) {
    var allocationType, actionType, requirements, docs;

    allocationType = scope.allocationType.toLowerCase().replace(/\s/g, '_');
    actionType = scope.actionType.toLowerCase();

    Api.Requests.one().one('requirements').get().then(function(resp) {
      requirements = resp[allocationType];
      scope.reqs = _.keys(requirements[actionType]);

      docs = requirements[actionType].documents || [];
      if (docs.length > 0) {
        Api.DocTypes.getList().then(function(list) {
          scope.docs = _.chain(list)
            .filter(function(docType) {
              return _.contains(docs, docType.documentType);
            })
            .pluck('displayDocumentType')
            .value();
        });
      }
    });
  };

  return {
    restrict: 'A',
    link: handler,
    scope: {
      allocationType: '=',
      actionType: '='
    },
    templateUrl: 'views/xrasRequirements.html'
  };
})
.directive('xrasPerson', function() {
  return {
    restrict: 'A',
    scope: {
      person: '='
    },
    templateUrl: 'views/xrasPerson.html'
  };
});
